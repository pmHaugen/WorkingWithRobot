# Robot Assembly

Assembling a Nvidia JETANK [Jetank AI KIT](https://www.waveshare.com/jetank-ai-kit.htm).

## Intro

This project was to asseble a robot that contains different electronics, motors, camera and a microcontroller

## Contents

* [Starting out](#starting-out)
* [Overview](#overview)
* [Halfway point](#halfway-point)
* [Mounting the arm](#mounting-the-arm)
* [SMILE!](#smile!)


## Starting out
First off mounted the servo motors and then I installed the circuit board that connects batteries and servo to the micro controller. As well as mounting the wifi antennas.

![Image1](Photos/1.jpg)


## Overview
Here is a picture of how it looks installed. 

![Image2](Photos/2.jpg)



## Halfway point
Now I mounted the microcontroller and installed the wifi board with heatsink and fan. Here I also connected all the wires to the wifi antenas, camera, motors and the circut board

![Image3](Photos/3.jpg)



## Mounting the arm
This is a picture of the completed robot. After the last picture I cable managed all the cables through the designated opening and then mounted the cabinet and arm mount. I then put together and wired the arm, motors and the camera so to complete the robot. 

![Image4](Photos/4.jpg)



## SMILE!
It seems like the robot came pre programmed with some evil software that makes it try to steal noses. But I faked a smile as the assignment wanted me to. I did have fun but I have not seen my nose since this day. 

![Image5](Photos/5.jpg)



## Contribute

 Paal Marius Bruun Haugen 
